+++

title = "actionable insights"

description = "SMART goes beyond colorful charts and exotic KPIs. It uses real-world connections to deliver insight into your local market ERP systems and staffing needs. It also helps you engage customers and solve problems before they know they have them. That's SMART."

template = "page.html"

slug = "smart"

+++
