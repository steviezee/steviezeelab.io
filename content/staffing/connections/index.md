+++

title ="Now you get it."

description ="SMART gives you a methodology that connects the staffing AND systems industries providing insight to the needs of your customers, creating a clear credibility differentiator and lowering customer acquisition costs. That’s being connected. That’s being SMART. Want more? Contact us."

template = "page.html"

slug = "connections"

in_search_index = true

render = true

+++
